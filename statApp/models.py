from django.db import models
from django.utils.timezone import now
from jsonfield import JSONField
import uuid


class SharedPage(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=120)
    grades_table = models.TextField()
    subjects = JSONField()
    monthly_averages = JSONField()
    subjects_with_number_of_grades = JSONField()
    data_date = models.DateTimeField(default=now)

    def __str__(self):
        return str(self.title)
