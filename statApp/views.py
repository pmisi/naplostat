from django.shortcuts import render, get_object_or_404
from django.views import View
from django.http import HttpResponse
from django.urls import reverse_lazy
from robobrowser import RoboBrowser
from .models import SharedPage
import re
import bs4
import datetime


# üdvözlő oldal, csak renderel
class IndexView(View):
    def get(self, request, *args, **kwargs):
        background_url = 'https://i0.wp.com/www.oxbridgeacademy.edu.za/blog/wp-content/uploads/2015/07/CN63QSUO8C.jpg?resize=1280%2C640&ssl=1'
        context = {
            'title': 'A motiváló osztályzatstatisztika',
            'title_img_src': background_url,
            'title_img_color': '(10.2, 13.7, 49.4)',  # a háttérkép domináns színe
            'title_img_color_half_opacity': '(10.2, 13.7, 49.4, 0.5)',
        }
        return render(request, 'statApp/index.html', context)


# POST lekérést követően létrehozza a scraper objektumot,
# aminek segítségével scrapel különböző adatokat a naplóból
class ellenorzoLoginView(View):
    def post(self, request, *args, **kwargs):
        self.user_name = request.POST.get('user_name')
        self.user_password = request.POST.get('user_password')
        self.user_server = request.POST.get('user_server')

        self.br = self.log_into_moza()

        self.moza_name = self.get_name()

        self.br.open(self.grades_url)

        grades_table = self.get_grades_table()
        subjects = self.get_subjects_with_averages()
        monthly_averages = self.get_subjects_with_monthly_averages()
        subjects_with_number_of_grades = self.get_subjects_with_number_of_grades()
        title = self.moza_name + ' ellenőrzője'

        background_url = 'http://blog.mindlogr.com/wp-content/uploads/2013/01/diary1.jpg'
        background_color = (201, 168, 131)  # a háttérkép domináns színe
        background_color_transparent = list(background_color)
        background_color_transparent.append(0.5)
        background_color_transparent = tuple(background_color_transparent)

        shared_page, created = SharedPage.objects.get_or_create(title=title)
        shared_page.grades_table = grades_table
        shared_page.subjects = subjects
        shared_page.monthly_averages = monthly_averages
        shared_page.subjects_with_number_of_grades = subjects_with_number_of_grades
        shared_page.data_date = datetime.datetime.now()

        shared_page.save()

        if not created:
            updated = True
        else:
            updated = False

        context = {
            'title_img_src': background_url,
            'title_img_color': background_color,
            'title_img_color_half_opacity': background_color_transparent,
            'title': title,
            'secondary_title': 'Bejelentkezve mint: ' + self.user_name,
            'grades_table': grades_table,
            'subjects': subjects,
            'monthly_averages': monthly_averages,
            'subjects_with_number_of_grades': subjects_with_number_of_grades,
            'updated': updated,
            'created': created,
        }
        return render(request, 'statApp/ellenorzo.html', context)

    # létrehozza a böngészőobjektumot, belép a naplóba és
    # visszaadja a beléptetett böngészőt
    def log_into_moza(self):
        br = RoboBrowser(history=False)
        br.open(self.user_server)

        login_form = br.get_form(action='user.php?cmd=login')
        login_form['loginname'] = self.user_name
        login_form['password'] = self.user_password

        br.submit_form(login_form)

        self.grades_url = self.user_server + '/' + br.get_link('Ellenőrzők, bizonyítványok').get_attribute_list('href')[
            0]

        return br

    # elnavigál az 'adatok' oldalra, ahonnan összeadva visszaadja a tanuló nevét
    # a visszaadást megelőzően visszatér a napló főoldalára
    def get_name(self):
        self.br.open(self.user_server + '/' + self.br.get_link('Adatok').get_attribute_list('href')[0])
        first_name = self.br.select('#db_viselt_elonev')[0].get_attribute_list('value')[0]
        last_name = self.br.select('#db_szuleteskori_utonev')[0].get_attribute_list('value')[0]

        self.br.open(self.user_server)

        return first_name + ' ' + last_name

    # elnavigál az 'ellenőrzők' oldalra és visszaadja a jegyek táblázatát materializenak megfelelő szintaxissal
    def get_grades_table(self):

        table = str(self.br.select('#statistics')[0])
        table = table.replace('lista_tabla', 'striped centered responsive-table')
        table = table.replace(self.moza_name + ' ellenőrzője', '')  # már kiírtuk a címben, kinek a jegytáblázata ez

        return table

    # elnavigál az ellenőrzőhöz és stringgé formálva visszaad egy listát,
    # amiben a tantárgyak és átlagai listában tárolódnak
    def get_subjects_with_averages(self):

        subjects = self.br.select('.haladasi_inner')
        subjects_with_average = {}
        for subject in subjects:
            if self.cleanhtml(str(subject.findChildren()[-1])).rstrip(' ') != "":  # ha nincs jegy, ne vegyük figyelembe
                subject_name = str(subject.findChild().text)
                subject_average = self.cleanhtml(str(subject.findChildren()[-1])).replace(',', '.')
                subjects_with_average[subject_name] = subject_average

        return subjects_with_average

    # kiveszi egy stringből a html tageket
    def cleanhtml(self, raw_html):
        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', raw_html)
        return cleantext

    # visszaadja a tanárgyak jegyeit listák listájának dictjében
    # a listába rendezett jegyek a hónapot jelentik
    def get_subjects_with_monthly_averages(self):

        subjects = self.br.select('.haladasi_inner')
        subjects_with_monthly_averages = {}

        # szorzók
        piros = 2
        fekete = 1
        zold = 0.5
        kek = 0.5

        for subject in subjects:
            subjects_with_monthly_averages[subject.findChild().text.title()] = []
            for month in subject.findChildren('td'):
                if month.text != subject.findChild().text and str(month.text).rstrip(' ') != '' and \
                        month.get_attribute_list('id')[0] != 'atlag_':

                    month_grade_list = []
                    for month_child in month.findChildren():
                        if type(month_child) is not bs4.element.NavigableString:

                            if str(month_child.get_attribute_list('style')[0]).split(' ')[
                                0] is None:  # fekete (egész érték)
                                self.append_to_month_grade_list(month_child, fekete, month_grade_list)

                            elif str(month_child.get_attribute_list('style')[0]).split(' ')[
                                0] == 'color:#221e1f;':  # fekete (egész érték)
                                self.append_to_month_grade_list(month_child, fekete, month_grade_list)

                            elif str(month_child.get_attribute_list('style')[0]).split(' ')[
                                0] == 'color:#3166FF;':  # kék (fél érték)
                                self.append_to_month_grade_list(month_child, kek, month_grade_list)

                            elif str(month_child.get_attribute_list('style')[0]).split(' ')[
                                0] == 'color:#33aa00;':  # zöld (fél érték)
                                self.append_to_month_grade_list(month_child, zold, month_grade_list)

                            elif str(month_child.get_attribute_list('style')[0]).split(' ')[
                                0] == 'color:#ff1100;':  # piros (dupla érték)
                                self.append_to_month_grade_list(month_child, piros, month_grade_list)

                            elif str(month_child.get_attribute_list('style')[0]).split(' ')[
                                0] == 'color:#32cb00;':  # világoszöld (fél érték)
                                self.append_to_month_grade_list(month_child, zold, month_grade_list)

                            elif str(month_child.get_attribute_list('style')[0]).split(' ')[
                                0] == 'color:#000000;':  # fekete (egész érték)
                                self.append_to_month_grade_list(month_child, fekete, month_grade_list)

                            else:
                                self.append_to_month_grade_list(month_child, zold, month_grade_list)

                    subjects_with_monthly_averages[subject.findChild().text.title()].append(month_grade_list)

        return subjects_with_monthly_averages

    # a get_subjects_with_monthly_averages metódushoz használatos
    # hozzáad egy elemet a megfelelő súlyozással a month_grade_listhez
    def append_to_month_grade_list(self, month_child, color_value, month_grade_list):
        for i in range(int(color_value * 2)):
            month_grade_list.append(str(month_child.text).replace('\xa0', ''))

    # listák listáját adja vissza, ami a tantárgy nevét és annak jegyeinek (súlyozás nélkül) tartalmazza
    def get_subjects_with_number_of_grades(self):
        subjects = self.br.select('.haladasi_inner')
        subjects_with_number_of_grades = {}
        for subject in subjects:
            subject_grade_number = 0
            if self.cleanhtml(str(subject.findChildren()[-1])).rstrip(' ') != "":  # ha nincs jegy, ne vegyük figyelembe
                for month in subject.findChildren('td'):
                    if month.text != subject.findChild().text and str(month.text).rstrip(' ') != '' and \
                            month.get_attribute_list('id')[0] != 'atlag_' and '\xa0' not in month.text:
                        subject_grade_number += len(month.text.split(' '))

            subjects_with_number_of_grades[subject.findChild().text.title()] = subject_grade_number

        return subjects_with_number_of_grades


class Share(View):
    def post(self, request):
        title = request.POST.get('title')
        grades_table = request.POST.get('grades_table')
        subjects = request.POST.get('subjects')
        monthly_averages = request.POST.get('monthly_averages')
        subjects_with_number_of_grades = request.POST.get('subjects_with_number_of_grades')

        try:
            shared_page = SharedPage.objects.get(
                title=title,
            )

        except SharedPage.DoesNotExist:
            shared_page = SharedPage(
                title=title,
                grades_table=grades_table,
                subjects_with_averages=subjects,
                monthly_averages=monthly_averages,
                subjects_with_number_of_grades=subjects_with_number_of_grades
            )

            shared_page.save()

        return HttpResponse(reverse_lazy('megosztott_ellenorzo', kwargs={'uuid': str(shared_page.uuid)}))


class SharedView(View):
    def get(self, request, uuid):
        shared_page = get_object_or_404(SharedPage, uuid=uuid)

        background_url = 'http://blog.mindlogr.com/wp-content/uploads/2013/01/diary1.jpg'
        background_color = (201, 168, 131)  # a háttérkép domináns színe
        background_color_transparent = list(background_color)
        background_color_transparent.append(0.5)
        background_color_transparent = tuple(background_color_transparent)

        context = {
            'title_img_src': background_url,
            'title_img_color': background_color,
            'title_img_color_half_opacity': background_color_transparent,

            'title': shared_page.title,
            'grades_table': shared_page.grades_table,
            'subjects': shared_page.subjects,
            'monthly_averages': shared_page.monthly_averages,
            'subjects_with_number_of_grades': shared_page.subjects_with_number_of_grades,

            'secondary_title': shared_page.data_date.strftime('%Y.%m.%d. %H:%M'),
            'shared_page': True
        }

        return render(request, 'statApp/ellenorzo.html', context)
