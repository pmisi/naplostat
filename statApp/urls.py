from django.urls import path
from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('ellenorzo', views.ellenorzoLoginView.as_view(), name='ellenorzo_login'),
    path('megosztas', views.Share.as_view(), name='megosztas'),
    path('ellenorzo/<str:uuid>', views.SharedView.as_view(), name='megosztott_ellenorzo')

]
