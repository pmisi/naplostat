<h1>Naplóstat</h1>

##### Egy szebb és részletesebb osztályzatstatisztika rendszer

Django webszerver, ahol a felhasználó adatai megadása után parseolja a felhasználó MozaNaplóban szereplő jegyét.

Használt technológiák:
- Django - webszerver
- [Robobrowser](https://github.com/jmcarp/robobrowser "Robobrowser") - weboldal parseolása
- [Materializecss](https://materializecss.com/ "Materializecss") - kliensoldali designkeretrendszer